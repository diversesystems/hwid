import java.security.MessageDigest;
import java.util.Map;

public class HWID {

    public static void main(String[] args) {
        try {
            StringBuilder s = new StringBuilder();

            // System.out.println("System Environment");
            // for(Map.Entry e : System.getenv().entrySet())
            // System.out.println(e.getKey() + " " + e.getValue());

            // System.out.println("\nSystem Properties");
            // for(Map.Entry e : System.getProperties().entrySet())
            // System.out.println(e.getKey() + " " + e.getValue());

            s.append(System.getenv("PROCESSOR_ARCHITECTURE").trim())
                    .append(System.getenv("PROCESSOR_IDENTIFIER").trim()).append(System.getenv("COMPUTERNAME").trim())
                    .append(System.getenv("NUMBER_OF_PROCESSORS"));

            s.append(System.getProperty("os.name").trim()).append(System.getProperty("os.version").trim())
                    .append(System.getProperty("os.arch").trim()).append(System.getProperty("user.country"))
                    .append(System.getProperty("user.name").trim());

            // could add Minecraft name and UUID? maybe MAC address?

            System.out.println(s.toString());

            byte[] b = s.toString().getBytes("UTF-8");

            /*
             * MD2 MD5 SHA-1 SHA-256 SHA-384 SHA-512
             */

            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] md5 = md.digest(b);

            for (byte y : md5)
                System.out.print(Integer.toHexString((y & 0xFF)));
            // increase or add some "randomness" to the hex string with by bitwise or'ing |
            // a hex number
            // System.out.print(Integer.toHexString((y & 0xFF) | 0x300) + " ");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}